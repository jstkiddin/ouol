package entity;
import javax.persistence.*;


@Entity
@Table(name = "anime_list")
public class Animu implements IAnimu{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    @Column(name = "name")
    private String name_a; //название анимешки
    @Column(name = "year")
    private int year; // год выпуска
    @Column(name = "ganre")
    private String ganre; //жанр, собсно
   // private int audince; //ограничение по возрасту
    @ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    private User user;

    public Animu(String name_a, int year, String ganre){
     //   this.audince = audience;
        this.ganre = ganre;
        this.name_a = name_a;
        this.year = year;
    }

    public int getYear() {
        return year;
    }

    public String getName_a() {
        return name_a;
    }

    public String getGanre() {
        return ganre;
    }

    public void setName_a(String name_a) {
        this.name_a = name_a;
    }

    public void setGanre(String ganre) {
        this.ganre = ganre;
    }

    public void setYear(int year) {
        this.year = year;
    }

    /*public void setAudince(int audince) {
        this.audince = audince;
    }

    public int getAudince() {
        return audince;
    }*/

    public User getUser(){return user;}

    public void setUser(User user){this.user = user;}

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}