package entity;
import javax.persistence.*;
/**
 * Created by Makoto on 02.09.2016.
 */
@Entity
@Table(name = "users")
public class User implements IUser {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    @Column(name = "logname")
    private String log_name; //имя user
    @Column(name = "password")
    private String pass;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "id")
    private Animu anime;

    public User(String log_name, String pass,Animu anime) {
        this.log_name = log_name;
        this.pass = pass;
        this.anime = anime;
    }

    public long getId() {
        return id;
    }

    public String getLog_name() {
        return log_name;
    }

    public String getPass() {
        return pass;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setLog_name(String log_name) {
        this.log_name = log_name;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public Animu getAnime (){
        return anime;}

    public void setAnime(Animu anime) {
        this.anime = anime;}
}