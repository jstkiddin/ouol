package db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.text.DateFormat;
import java.util.Date;

/**
 * Created by Makoto on 02.09.2016.
 */
public class DBWorker {
    private String DB_DRIVER = "com.mysql.jdbc.Driver";
    private String DB_USER = "root";
    private String DB_PASSWORD = "root";
    private String DB_CONNECTION = "jdbc:mysql://localhost:3306/OrganizeUrOtakuLife";
    //jdbc:mysql://hostname:port/dbname
    private static int id = 1;

    private static String getCurrentTimeStamp() {
        Date today = new Date();
        DateFormat dateFormat = DateFormat.getDateInstance();
        return dateFormat.format(today.getTime());
    }

    private Connection getDBConnection() {
        Connection dbConnection = null;
        try {
            Class.forName(DB_DRIVER);
        } catch (ClassNotFoundException e) {
            System.out.println(e.getMessage());
        }
        try {
            dbConnection = DriverManager.getConnection(DB_CONNECTION, DB_USER, DB_PASSWORD);
            return dbConnection;
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return dbConnection;
    }



}
